# README #

### What is this repository for? ###

* Tune OSX to provide useful and performant default values
* See https://support.apple.com/en-ca/HT205926

### Deployment ###

1. Open Application "Terminal"
2. Type the commands below (will prompt for the password of your Mac)

```sudo curl -H 'Cache-Control: no-cache' -L -s https://bitbucket.org/raptus-it-services/sane-osx-defaults/raw/master/sane-osx-defaults.sh | bash```


### Troubleshooting
* tbd

### Todo

* tbd
