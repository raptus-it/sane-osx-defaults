#!/bin/bash

echo "Updating /etc/nsmb.conf"
sudo curl -o /etc/nsmb.conf https://bitbucket.org/raptus-it-services/sane-osx-defaults/raw/master/nsmb.conf >>/dev/null 2>&1

echo "Updating /etc/sysctl.conf"
sudo curl -o /etc/sysctl.conf https://bitbucket.org/raptus-it-services/sane-osx-defaults/raw/master/sysctl.conf >>/dev/null 2>&1

echo "Switching off SMB signging"
sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server SigningRequired -bool FALSE

echo "Switching off DS files on shares"
sudo defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool TRUE

echo
echo "You should reboot your Mac"

# eof
